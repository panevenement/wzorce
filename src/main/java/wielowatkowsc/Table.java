package wielowatkowsc;

public class Table {
    void printTable(int n) {
        for (int i = 1; i <= 5; i++) {
            System.out.println(n * i);
            try {
                Thread.sleep(400);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
            synchronized (this) {//method not synchronized
                for (int x = 1; x <= 5; x++) {
                    System.out.println(n * x);
                    try {
                        Thread.sleep(400);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
        }
    }