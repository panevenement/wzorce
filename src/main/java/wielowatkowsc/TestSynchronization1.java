package wielowatkowsc;

public class TestSynchronization1 {
    public static void main(String args[]) {
        Table obj = new Table();//only one object
        ActionThread1 t1 = new ActionThread1(obj);
        ActionThread2 t2 = new ActionThread2(obj);
        t1.start();
        t2.start();
    }
}