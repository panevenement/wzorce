package Wzroce_Projektowe.Adapter.AdapterV2;

public class PayPalWrapper {

    private PayPalV2 payPalV2;

    public PayPalWrapper(){
        payPalV2 = new PayPalV2();
    }

    public void pay(float payment){
        payPalV2.payment(payment);
    }
}
