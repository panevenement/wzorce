package Wzroce_Projektowe.Adapter;

public class HeartBeatWrapper {

    private HeartBeatV2 heartBeatV2;

    public HeartBeatWrapper(){
        heartBeatV2 = new HeartBeatV2();
    }
    public void trackPlay(){
        heartBeatV2.trackEvent(EventTime.PLAY);
    }
    public void trackPause(){
        heartBeatV2.trackEvent(EventTime.PAUSE);
    }
    public void trackSessionStart(){
        heartBeatV2.trackSessionStart();
    }
}
