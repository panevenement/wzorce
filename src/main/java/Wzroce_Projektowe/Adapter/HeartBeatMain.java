package Wzroce_Projektowe.Adapter;

public class HeartBeatMain {

    public static void main(String[] args) {
        HeartBeatWrapper heartBeatWrapper = new HeartBeatWrapper();

        heartBeatWrapper.trackPlay();
        heartBeatWrapper.trackPause();
        heartBeatWrapper.trackSessionStart();
    }

}
