package Wzroce_Projektowe.Fasada.Car;

public class Car {

    private Enginie enginie;
    private Break aBreak;
    private Clutch clutch;
    private Lights lights;

    public Car(){
        this.enginie = new Enginie();
        this.aBreak = new Break();
        this.clutch = new Clutch();
        this.lights = new Lights();

    }

    public void carStart(){
        clutch.pushClutch();
        aBreak.pushBreak();
        enginie.enginieStart();
        lights.lightOn();

    }
}
