package Wzroce_Projektowe.Builder;

public abstract class HouseBuilder {

    protected String windowsType;
    protected String houseName;

    public HouseBuilder setWindowsType(String windowsType){
        this.windowsType = windowsType;
        return this;
    }

    public HouseBuilder setHouseName(String houseName){
        this.houseName = houseName;
        return this;
    }

    public abstract House build();

    @Override
    public String toString() {
        return "HouseBuilder{" +
                "windowsType='" + windowsType + '\'' +
                ", houseName='" + houseName + '\'' +
                '}';
    }
}

