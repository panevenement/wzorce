package Wzroce_Projektowe.Builder;

public class HouseMain {
    public static void main(String[] args) {
        HouseBuilder houseBuilder = new
                WoodenHouseBuilder();
        houseBuilder.setHouseName("wooden house");
        houseBuilder.setWindowsType("wooden");
        House house = houseBuilder.build();
        System.out.println(house);
    }
}
