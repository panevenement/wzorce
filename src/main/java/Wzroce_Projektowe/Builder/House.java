package Wzroce_Projektowe.Builder;

public class House {
    String wallType;
    String windowsType;
    String roof;
    String houseName;
    public House(String wallType, String windowsType, String roof, String houseName) {
        this.wallType = wallType;
        this.windowsType = windowsType;
        this.roof = roof;
        this.houseName = houseName;
    }

    @Override
    public String toString() {
        return "House{" +
                "wallType='" + wallType + '\'' +
                ", windowsType='" + windowsType + '\'' +
                ", roof='" + roof + '\'' +
                ", houseName='" + houseName + '\'' +
                '}';
    }
}
