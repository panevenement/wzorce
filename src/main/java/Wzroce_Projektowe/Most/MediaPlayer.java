package Wzroce_Projektowe.Most;

public class MediaPlayer implements PlayerApi {


    @Override
    public void play(String track) {
        System.out.println("Leci utwór z Media Plejera " + track);

    }

    @Override
    public void pause(String track) {
        System.out.println("Pałzujesz utwór z Media Plajera " +track);
    }
}
