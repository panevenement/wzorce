package Wzroce_Projektowe.Most;

public class MostMain {
    public static void main(String[] args) {

        MediaPlayer mediaPlayer = new MediaPlayer();
        SdaStreamPlatform streamPlatform = new SdaStreamPlatform("MediaPlayer",mediaPlayer);

        streamPlatform.setTrack("Wikingowie");
        streamPlatform.play("Wikingowie");

        ExoPlayer exoPlayer = new ExoPlayer();
        streamPlatform.setPlayerApi(exoPlayer);

        streamPlatform.play("Wikingowie");
    }


}
