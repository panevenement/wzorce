package Wzroce_Projektowe.Most;

public class SdaStreamPlatform extends VodPlatform {

    private String platformName;
    private PlayerApi playerApi;

    public SdaStreamPlatform(String platformName, PlayerApi playerApi){
        this.platformName=platformName;
        this.playerApi=playerApi;
    }

    public void setTrack(String track){
        super.setTrack(track);
    }
    public void play(String track){
        playerApi.play(track);
    }
    public void pause(String track){
        playerApi.pause(track);
    }

    public void setPlayerApi(PlayerApi playerApi){
        this.playerApi=playerApi;
    }
}
