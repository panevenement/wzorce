package Wzroce_Projektowe.Kompozyt.Folders;

public class FolderMain {
    public static void main(String[] args) {


        Catalog myComputer = new Catalog("Mój komputer(Katalog)");
        Catalog myDocuments = new Catalog("Moje Dokumenty(Katalog)");
        Catalog myPictures = new Catalog("Moje Zdjęcia(Katalog)");
        Catalog myMusic = new Catalog("Moja Muzyka(Katalog)");

        myDocuments.addFile(new File("Zadania domowe.txt"));
        myDocuments.addFile(new File("Zadania na SDA.txt"));
        Catalog projektySDA = new Catalog("Projekty(Katalog)");
        myDocuments.addFile(projektySDA);
        projektySDA.addFile(new File("Wzorce Projektowe.pdf"));
        Catalog zadania12 = new Catalog("Zadania 12(Katalog)");
        projektySDA.addFile(zadania12);
        zadania12.addFile(new File("Zadania z Kompozytów.pdf"));
        myPictures.addFile(new File("Zdjęcia z wakacji123.pdf"));

        myComputer.addFile(myDocuments);
        myComputer.addFile(myPictures);
        myComputer.addFile(myMusic);
        myComputer.showMe();


    }
}
