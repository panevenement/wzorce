package Wzroce_Projektowe.Kompozyt.Folders;

import java.util.ArrayList;
import java.util.List;

public class Catalog extends FileAbstract {

    private List<FileAbstract> catalogs = new ArrayList<>();
    private String name;

    public Catalog(String name){
        this.name=name;
    }

    @Override
    protected String getFileName() {
        return name;
    }

    @Override
    public void showMe(){
        super.showMe();
        for(FileAbstract catalog : catalogs){
            catalog.showMe();
        }
    }

    @Override
    public void addFile(FileAbstract fileAbstract) {
        catalogs.add(fileAbstract);
    }

    @Override
    public void removeFile(FileAbstract fileAbstract) {
        catalogs.remove(fileAbstract);
    }

}
