package Wzroce_Projektowe.Kompozyt.Folders;

public abstract class FileAbstract {

    protected abstract String getFileName();

    public void showMe(){
        System.out.println("File: " +this.getFileName());
    }

    public abstract void addFile(FileAbstract fileAbstract);
    public abstract void removeFile(FileAbstract fileAbstract);



}
