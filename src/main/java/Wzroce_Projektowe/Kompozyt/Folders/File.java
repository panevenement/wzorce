package Wzroce_Projektowe.Kompozyt.Folders;

public class File extends FileAbstract {

    private String name;

    public File(String name){
        this.name=name;
    }


    @Override
    protected String getFileName() {
        return name;
    }

    @Override
    public void addFile(FileAbstract fileAbstract) {
        System.out.println("Plik Wykonywalny, nie można dodać");
    }

    @Override
    public void removeFile(FileAbstract fileAbstract) {
        System.out.println("Plik wykonywalny, nie można usunąć");
    }
}
