package Wzroce_Projektowe.Obserwator;

public interface Observer {
    void register(Subject subject);
    void unregister(Subject subject);
    void recive(String message);
}
