package Wzroce_Projektowe.Obserwator;

public class Costumer implements Observer {

    private String name;

    public Costumer(String name){
        this.name=name;
    }

    @Override
    public void register(Subject subject) {
        subject.register(this);
    }

    @Override
    public void unregister(Subject subject) {
        subject.unregister(this);
    }

    @Override
    public void recive(String message) {
        System.out.println(name+ ": "+message);
    }


}
