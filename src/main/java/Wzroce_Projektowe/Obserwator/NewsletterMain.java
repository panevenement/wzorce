package Wzroce_Projektowe.Obserwator;

public class NewsletterMain {
    public static void main(String[] args) {


        Subject subject = new MediaMarkt();

        Observer costumer1 = new Costumer("Adrian");
        Observer costumer2 = new Costumer("Krzychu");
        Observer costumer3 = new Costumer("Paweł");

        costumer1.register(subject);
        costumer2.register(subject);
        costumer3.register(subject);

        subject.notifyAll("Nowy Ajpad w przedsprzedaży!");

        costumer2.unregister(subject);

    }
}
