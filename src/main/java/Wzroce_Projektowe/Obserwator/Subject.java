package Wzroce_Projektowe.Obserwator;

public interface Subject {

    void register(Observer observer);
    void unregister(Observer observer);
    void notifyAll(String message);
}
