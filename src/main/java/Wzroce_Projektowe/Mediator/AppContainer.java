package Wzroce_Projektowe.Mediator;

import java.util.ArrayList;
import java.util.List;

public class AppContainer implements UIContainer {

    public List<Component> componentList = new ArrayList<>();

    @Override
    public void onComponentClick(Component component) {
        for(Component c : componentList){
            if(c != component){
                component.onClick(component);
            }
        }
    }

    @Override
    public void addComponent(Component component) {
        componentList.add(component);
    }

    @Override
    public String toString() {
        return "AppContainer{" +
                "componentList=" + componentList +
                '}';
    }
}
