package Wzroce_Projektowe.Mediator;

public class ButtonComponent extends Component {

    private UIContainer container;


    public ButtonComponent(UIContainer uiContainer, String name) {
        super(name);
        this.container = uiContainer;
    }

    @Override
    public void click() {
        System.out.println("Click on: " +container);
        container.onComponentClick(this);
    }

    @Override
    public void onClick(Component component) {
        System.out.println("Button " + component.name + " was Clicked");

    }

}
