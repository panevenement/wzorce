package Wzroce_Projektowe.Mediator;

public interface UIContainer {

    void onComponentClick(Component component);
    void addComponent(Component component);
}
