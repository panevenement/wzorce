package Wzroce_Projektowe.Mediator;



public class App {
    public static void main(String[] args) {
        UIContainer container = new AppContainer();
        Component button1 = new ButtonComponent(container,"wyślij");
        Component button2 = new ButtonComponent(container,"Pobierz");
        Component checkBox = new CheckBoxComponent(container,"checkbox");
        container.addComponent(button1);
        container.addComponent(button2);
        container.addComponent(checkBox);
        checkBox.click();
        button1.click();




    }
}
