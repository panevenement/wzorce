package Wzroce_Projektowe.Mediator;

public class CheckBoxComponent extends Component {

    public UIContainer container;

    public CheckBoxComponent(UIContainer container ,String name) {
        super(name);
        this.container=container;
    }

    @Override
    public void click() {
        System.out.println("Click on: " +name );
        container.onComponentClick(this);
    }

    @Override
    public void onClick(Component component) {
        System.out.println("Checkbox component: " + component.name);

    }
}
