package Wzroce_Projektowe.MetodaWytwórcza;

public class DeluxePizza extends Pizza {

    private double price = 10.5;

    @Override
    public double getPrice() {
        return price;
    }
}
