package Wzroce_Projektowe.MetodaWytwórcza;

public interface PizzaCreator {

    Pizza createPizza(PizzaType pizzaType);
}
