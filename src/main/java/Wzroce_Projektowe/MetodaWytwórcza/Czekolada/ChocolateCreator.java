package Wzroce_Projektowe.MetodaWytwórcza.Czekolada;

public interface ChocolateCreator {

   Chocolate createChocolate(ChocolateType chocolateType);
}
