package Wzroce_Projektowe.MetodaWytwórcza.Czekolada;

public class ChocolateFactory implements ChocolateCreator {



    @Override
    public Chocolate createChocolate(ChocolateType chocolateType) {

        switch (chocolateType){
            case Dark:
                return new DarkChocolate();
            case Milk:
                return new MilkChocolate();
            case Hazelnut:
                return new HazelnutChocolate();
        }
        throw new IllegalArgumentException("Chocolate Type " +chocolateType +" is not recognized.");

    }
}
