package Wzroce_Projektowe.MetodaWytwórcza.Czekolada;

public class DarkChocolate extends Chocolate {

    private String description = "Dark";

    @Override
    public String getDesciption() {
        return description;
    }


    @Override
    public String toString() {
        return "DarkChocolate{" +
                "description='" + description + '\'' +
                '}';
    }
}


