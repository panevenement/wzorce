package Wzroce_Projektowe.MetodaWytwórcza.Czekolada;

public class MilkChocolate extends Chocolate {

    private String description = "Milk";


    @Override
    public String getDesciption() {
        return description;
    }

    @Override
    public String toString() {
        return "MilkChocolate{" +
                "description='" + description + '\'' +
                '}';
    }
}
