package Wzroce_Projektowe.MetodaWytwórcza.Czekolada;

public class ChocolateMain {
    public static void main(String[] args) {


        ChocolateFactory chocolateFactory = new ChocolateFactory();

        System.out.println(chocolateFactory.createChocolate(ChocolateType.Dark).getDesciption());
        System.out.println(chocolateFactory.createChocolate(ChocolateType.Milk).getDesciption());
        System.out.println(chocolateFactory.createChocolate(ChocolateType.Hazelnut).getDesciption());

    }
}
