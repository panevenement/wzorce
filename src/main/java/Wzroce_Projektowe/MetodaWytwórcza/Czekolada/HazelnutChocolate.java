package Wzroce_Projektowe.MetodaWytwórcza.Czekolada;

public class HazelnutChocolate extends Chocolate {

    private String description = "Hazelnuts";

    @Override
    public String getDesciption() {
        return description;
    }
}
