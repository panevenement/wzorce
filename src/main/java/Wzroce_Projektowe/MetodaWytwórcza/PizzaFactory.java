package Wzroce_Projektowe.MetodaWytwórcza;

public class PizzaFactory implements PizzaCreator{



    @Override
    public Pizza createPizza(PizzaType pizzaType) {

        switch (pizzaType){
            case HamMushroom:
                return new HamAndMushroomPizza();
            case Deluxe:
                return new DeluxePizza();
            case Pepperoni:
                return new PepperoniPizza();
        }
        throw new IllegalArgumentException("The Pizza type " + pizzaType + " is not recognized.");

    }

}
