package Wzroce_Projektowe.MetodaWytwórcza;

public class PepperoniPizza extends Pizza {

    private double price = 29.9d;

    @Override
    public double getPrice() {
        return price;
    }
}
